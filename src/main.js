'use strict'

// module dependencies
const Koa = require('koa')

// homemade tools
const Logger = require('./tools/logger')('main')

// http endpoints register
const httpEndpointsRegister = require('./http-endpoints')

// create koa app
const app = new Koa()

// access detail logger
app.use(async (ctx, next) => {
  const start = Date.now()
  // wait next middware & receive error if exists.
  const error = await next()
  const duration = Date.now() - start
  // ex. `[2017/04/16 03:08:02 +0900] "GET" 404 "::1" "/" "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36"`
  Logger.log(`"${ctx.method}" ${ctx.status} "${ctx.ip}" "${ctx.url}" "${ctx.headers['user-agent']}"`)
  // show error stack if error exists.
  if (error) {
    // expose flag set, show in verbose mode only.
    const logLevel = error.expose ? 'detail' : 'error'
    Logger[logLevel](error.stack)
  }
  // set responce time header & show it in verbose mode.
  Logger.detail(`responded in ${duration} ms`)
  ctx.set('X-Responce-Time', `${duration} ms`)
})

// error handler
app.use(async (ctx, next) => {
  try {
    await next()
  } catch (e) {
    ctx.status = e.expose ? e.status : 500
    ctx.body = {
      message: e.expose ? e.message : 'an unexpected error has occurred, please contact to operators.'
    }
    // return error
    return e
  }
})

// 404 handler
app.use(async (ctx, next) => {
  await next()
  // if not body specified & status is 404, set not found message.
  if (!ctx.body && ctx.status === 404) {
    ctx.status = 404
    ctx.body = {
      message: 'there is no contents.'
    }
  }
})

// register http-endpoints
httpEndpointsRegister(app)

// 8080
app.listen(8080)
