/**
 * route of http-endpoints.
 */
module.exports = {
  get: [
    {
      name: '/test',
      processor: 'test',
      method: 'index'
    },
    {
      name: '/test/:id',
      processor: 'test',
      method: 'show'
    }
  ]
}
