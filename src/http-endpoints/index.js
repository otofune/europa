'use strict'

// module dependencies
const route = require('koa-route')
const path = require('path')

// homemade tools
const Logger = require('../tools/logger')('http-endpoints')

// get endpoints directory path
const endpointsDirectoryPath = path.dirname(__filename)

// get endpoints route json
const endpointsRoute = require('./route')

/**
 * custom error for endpoint register error handling
 * @param {string} message error message
 */
class RegisterError extends Error {
  constructor (message) {
    super(message)
    this.name = 'RegisterError'
  }
}

/**
 * wrap function to make it simple,
 * returned object of wrapped function will assign to ctx.body.
 * @param {function} func function to be wrapped
 * @return {function} wrapper function
 */
const wrap = (func) => {
  return async (ctx, ...args) => {
    // remove next from args
    const next = args.pop()
    // run func with args without ctx & next
    const body = await func(...args)
    // if body doesn't exist, kick next()
    body ? ctx.body = body : await next()
  }
}

/**
 * register http-endpoints to koa app
 * @param {object} app koa.js app
 */
module.exports = (app) => {
  // get http method key from endpointsRoute & loop with method name
  Object.keys(endpointsRoute).forEach((httpMethod) => {
    // loop with every endpoint definition
    endpointsRoute[httpMethod].forEach((endpoint) => {
      try {
        const processorPath = path.join(endpointsDirectoryPath, 'processors', endpoint.processor)
        const processor = require(processorPath)
        const method = processor[endpoint.method]
        if (!method) throw new RegisterError(`method "${endpoint.method}" doesn't exist in "${processorPath}".`)
        if (typeof method !== 'function') throw new RegisterError(`"${endpoint.method}" in "${processorPath}" is not a function.`)
        const wrapper = wrap(method)
        app.use(
        // with some reason, func in class isn't binded by class, so bind it
          route[httpMethod](endpoint.name, wrapper)
        )
      } catch (e) {
        // if not RegiserError or require's System Error, re-throw. (this will stop program)
        if (!(e instanceof RegisterError || e.code === 'MODULE_NOT_FOUND')) throw e
        Logger.log(`"${httpMethod}" endpoint "${endpoint.name}" definition isn't valid, skip registration.`)
        Logger.error(e.stack)
      }
    })
  })
}
