'use strict'

const zp = require('./zero-padding')

/**
 * generate time string, similar to "2017/04/16 03:08:02 +0900".
 * @return {string}
 */
module.exports = () => {
  const date = new Date()
  const offsetMinutes = -date.getTimezoneOffset()
  const offsetReminder = offsetMinutes % 60
  const offsetHours = (offsetMinutes - offsetReminder) / 60
  return zp(date.getFullYear(), 2) + '/' +
    zp(date.getMonth() + 1, 2) + '/' +
    zp(date.getDate(), 2) + ' ' +
    zp(date.getHours(), 2) + ':' +
    zp(date.getMinutes(), 2) + ':' +
    zp(date.getSeconds(), 2) + ' ' +
    zp(offsetHours, 2, true) +
    zp(Math.abs(offsetReminder), 2)
}
