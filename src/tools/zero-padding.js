'use strict'

/**
 * do zero-padding.
 *
 * @param {number} num processing number
 * @param {number} length length after processing
 * @param {showPlusSign} boolean show plus (+) to return string when mejor num specified
 * @return {string}
 */
module.exports = (num, length, showPlusSign = false) => {
  if (!Number.isInteger(length) || !Number.isInteger(num)) {
    throw new Error('argument must be integer.')
  }
  const isNegative = num < 0
  const str = Math.abs(num).toString()
  const brevity = length - str.length
  if (brevity <= 0) return num.toString()
  return (isNegative ? '-' : showPlusSign ? '+' : '') + Array(brevity + 1).join('0') + str
}
