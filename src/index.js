'use strict'

// module dependencies
const cluster = require('cluster')

// homemade tools
const Logger = require('./tools/logger')(cluster.isMaster ? 'master' : 'worker')

// clustering option
const clustering = process.argv.indexOf('--clustering') !== -1

/**
 * signature
 */
if (cluster.isMaster) {
  console.log('> europa server')
  console.log('> https://gitlab.com/otofune/europa' + '\n')
}

/**
 * fork every cpu cores if clustering option enabled.
 */
if (cluster.isMaster && clustering) {
  const os = require('os')

  // cpu cores counter
  const cpuCores = os.cpus().length

  // worker exited, notice to console & re-fork.
  cluster.on('exit', (worker) => {
    Logger.log(`worker number ${worker.id} is down.`)
    cluster.fork()
  })

  // worker in prepare time, notice to console.
  cluster.on('online', (worker) => {
    Logger.log(`worker number ${worker.id} is online.`)
  })

  // fork workers each cpu cores.
  for (let i = 0; i < cpuCores; i++) {
    cluster.fork()
  }
} else {
  require('./main')
  // notice worker number in worker
  if (cluster.isWorker) Logger.log(`worker number ${cluster.worker.id} is ready.`)
}
