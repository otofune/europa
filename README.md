europa
=====
just an application programming interface of gyroma service.

Requirements
-----
- Node.js (>= 7.6)

Installation
-----
1. clone this repository.
2. run `npm install`.

Run
-----
normally, use simple `npm start`. in debugging use `npm run debug`.  
if do not want to use npm scripts, use `node ./src/index.js [options]`.

### Options
|name|description|
|:--|:--|
|--verbose|output more informations|
|--clustering|use cluster module & fork workers|

License
-----
MIT. More details in [LICENSE](LICENSE).
